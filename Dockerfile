FROM ubuntu:latest
RUN apt-get update && apt-get upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install apache2 php libapache2-mod-php -y 
COPY src/index.php /var/www/html/
COPY src/flag.html /var/www/html/
COPY src/start.sh /bin/
RUN chown -R $USER:$USER /var/www
RUN chmod +x /bin/start.sh
RUN rm /var/www/html/index.html
EXPOSE 1002
RUN sed -i 's/80/1002/g' /etc/apache2/ports.conf
CMD "/bin/start.sh"

