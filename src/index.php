<!DOCTYPE html>
<html lang="en">

<head>
    <title>My New Website</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://getbootstrap.com/docs/3.3/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

<?php 
 	 setcookie('admin', "False", time() + (86400 * 30), "/");
        

        if(!isset($_COOKIE['admin']))
	{
           
            $_COOKIE['admin'] = "False"; 
            $user = ((isset($_POST['user']) && !empty($_POST['user']))?$_POST['user']:'default');
            setcookie('user', $user, 1);
            $_COOKIE['user'] = $user;
        }

    ?>


    <div class="container">
        <div class="header">
            <nav>
                <ul class="nav nav-pills pull-right">
                    <li role="presentation" class="active"><a href="#">Home</a>
                    </li>
                    <li role="presentation"><a href="/unimplemented">Sign In</a>
                    </li>
                    <li role="presentation"><a href="/unimplemented">Sign Out</a>
                    </li>
                </ul>
            </nav>
            <h3 class="text-muted">My New Website</h3>
        </div>






         
        <!-- Categories: success (green), info (blue), warning (yellow), danger (red) -->
        
        
        <div class="alert alert-warning alert-dismissible" role="alert" id="myAlert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <!-- <strong>Title</strong> --> I&#39;m sorry it doesn&#39;t look like you are the admin or it&#39;s the incorrect time.
            </div>
      
        <div class="jumbotron">
            <p class="lead"></p>
 	<?php 
                if ($_COOKIE['admin'] == "True"){
                    echo '<p style="text-align:center; font-size:30px;"><b>flag</b></p>';
                }else{
                    echo '<p style="text-align:center; font-size:30px;"><b>No flag for you</b></p>';
                }
            ?>  

            <!-- This is a comment <p><a href="/flag.html" class="btn btn-lg btn-success btn-block"> Flag</a></p>-->
        </div>
    </div>
</body>

</html>
